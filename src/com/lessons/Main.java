package com.lessons;

import java.util.Locale;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int firstNum = new Random().nextInt(100);
        int secondNum = new Random().nextInt(100);
        int thirdNum = new Random().nextInt(100);
        System.out.println("My numbers: 1) " + firstNum + "; 2) " + secondNum + "; 3) " + thirdNum + ".");
        int firstPow = 0;
        int secondPow = 0;

        if (firstNum >= secondNum && thirdNum >= secondNum) {
            firstPow = firstNum * firstNum;
            secondPow = thirdNum * thirdNum;
            System.out.println("The biggest numbers: " + firstNum + ", " + thirdNum + ".");
        } else if (secondNum >= firstNum && thirdNum >= firstNum) {
            firstPow = secondNum * secondNum;
            secondPow = thirdNum * thirdNum;
            System.out.println("The biggest numbers: " + secondNum + ", " + thirdNum + ".");
        } else if (secondNum >= thirdNum && firstNum >= thirdNum) {
            firstPow = firstNum * firstNum;
            secondPow = secondNum * secondNum;
            System.out.println("The biggest numbers: " + firstNum + ", " + secondNum + ".");
        }
        System.out.println(firstPow + " + " + secondPow + " = " + (firstPow + secondPow) + ".");
    }

//    public static void main(String[] args) {
//
//        int[] randomNumbers = {new Random().nextInt(100),
//                new Random().nextInt(100),
//                new Random().nextInt(100)};
//
//        double finalResult;
//        double sqrFirstNum;
//        double sqrSecondNum;
//
//        final int firstIndexOfNum = 1;
//        final int secondIndexOfNum = 2;
//
//        // try to find the biggest numbers
//        for (int i = 0; i < randomNumbers.length; i++) {
//            for (int j = 0; j < randomNumbers.length - 1; j++) {
//                if (randomNumbers[j] > randomNumbers[j + 1]) {
//                    int num = randomNumbers[j];
//                    randomNumbers[j] = randomNumbers[j + 1];
//                    randomNumbers[j + 1] = num;
//                }
//            }
//        }
//
//
//        System.out.print("My numbers: ");
//        for (int i : randomNumbers) {
//            System.out.print(i + " ");
//        }
//        System.out.print(";\n");
//
//        sqrFirstNum = Math.pow(randomNumbers[firstIndexOfNum], 2);
//        sqrSecondNum = Math.pow(randomNumbers[secondIndexOfNum], 2);
//
//        System.out.println(randomNumbers[firstIndexOfNum] + "^2 = " + sqrFirstNum + ";");
//        System.out.println(randomNumbers[secondIndexOfNum] + "^2 = " + sqrSecondNum + ";");
//
//        finalResult = sqrFirstNum + sqrSecondNum;
//        System.out.printf(Locale.getDefault(), "%.0f + %.0f = %.0f.", sqrFirstNum, sqrSecondNum, finalResult);
//    }
}
